## HW Spark1


```sh
data = sc.textFile('movies.csv')
data.map(lambda x: x.split(',')[1]).flatMap(lambda x: x.split()).map(lambda x: (x,1)).reduceByKey(lambda a,b: a + b).takeOrdered(5, key=lambda x: -x[1])

[('of', 946), ('the', 911), ('The', 411), ('and', 328), ('in', 245)]
```