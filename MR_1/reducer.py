#!/bin/python3
import sys

previous_keyword = None
keyword_count = 0

for line in sys.stdin:
    keyword, one = line.split()
    one = int(one)
    
    if previous_keyword:
        if previous_keyword == keyword:
            keyword_count += one
        else:
            print(previous_keyword, keyword_count)
            previous_keyword = keyword
            keyword_count = one
    else:
        previous_keyword = keyword
        keyword_count = one

print(previous_keyword, keyword_count)



# previous_keyword = None
# keyword_count = 0

# with open ('sorted_123.txt') as f:
#     for i, line in enumerate(f):
#         keyword, one = line.split()
#         one = int(one)
        
#         if previous_keyword:
#             if previous_keyword == keyword:
#                 keyword_count += one
#             else:
#                 print(previous_keyword, keyword_count)
#                 previous_keyword = keyword
#                 keyword_count = one
#         else:
#             previous_keyword = keyword
#             keyword_count = one

# print(previous_keyword, keyword_count)