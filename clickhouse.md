```
SELECT UserID, COUNT(UserID) 
                FROM homework.metrika 
                GROUP BY UserID
                ORDER BY COUNT(UserID) DESC
                LIMIT 1
                

SELECT
    UserID,
    COUNT(UserID)
FROM homework.metrika
GROUP BY UserID
ORDER BY COUNT(UserID) DESC
LIMIT 1

Query id: bf009354-19f5-4e56-8483-a511cb8a0c0a

┌──────────────UserID─┬─count(UserID)─┐
│ 1313448155240738815 │          4439 │
└─────────────────────┴───────────────┘

1 rows in set. Elapsed: 0.020 sec. Processed 100.00 thousand rows, 800.00 KB (5.04 million rows/s., 40.28 MB/s.)
```
